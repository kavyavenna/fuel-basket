package creativeappsbyus.fuelbasket;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddVehicles.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddVehicles#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddVehicles extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    static DataBaseObject db;
    static SQLiteDatabase sqlDB;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public EditText carMake;
    public EditText model;
    public EditText modelVersion;
    public Spinner fuelType;
    public EditText carColour;
    public EditText licensePlate;
    public EditText notes;
    public Button saveVehicleDetails;
    JSONObject array;
    JSONObject versionsArrayObject;
    String carSelected;

    String carMakeArray[] = {"Maruti", "Hyundai", "Honda", "Toyota", "Tata", "Mahindra", "Ford",
            "Chevrolet", "Audi", "Volkswagen", "BMW", "Renault", "Mercedes Benz", "Nissan", "Skoda", "Datsun", "Fiat", "Bentley",
            "Jaguar", "Lamborghini", "Bugatti", "Aston Martin", "DC", "Ferrari", "Force", "ICML", "Isuzu", "Jeep", "Land Rover",
            "Maserati", "MINI", "Mitsubishi", "Polaris", "Porsche", "Premier", "Rolls Royce", "SsangYong", "Volvo"};
    String[] modelsArray;
    String[] versionsArray;
    private OnFragmentInteractionListener mListener;

    public AddVehicles() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddVehicles.
     */
    // TODO: Rename and change types and number of parameters
    public static AddVehicles newInstance(String param1, String param2) {
        AddVehicles fragment = new AddVehicles();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_add_vehicles, container, false);
        db = new DataBaseObject(getContext());
        sqlDB = db.getReadableDatabase();
        carMake = (EditText) v.findViewById(R.id.make);
        model = (EditText) v.findViewById(R.id.model);
        modelVersion = (EditText) v.findViewById(R.id.version);
        fuelType = (Spinner) v.findViewById(R.id.fuel_type);
        carColour = (EditText) v.findViewById(R.id.colour_car);
        licensePlate = (EditText) v.findViewById(R.id.license_plate);
        notes = (EditText) v.findViewById(R.id.notes);
        saveVehicleDetails = (Button) v.findViewById(R.id.save_vehicle_details);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, carMakeArray);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        /*carMake.setAdapter(spinnerArrayAdapter);

        carMake.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carSelected=carMake.getSelectedItem().toString();
                String[] carsList=readCarModels(carSelected);
                Log.e("jhgty", String.valueOf(carsList));
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, carsList);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                model.setAdapter(spinnerArrayAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
        carMake.setFocusableInTouchMode(false);
        carMake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(getContext())
                        .setTitle("Select Car Makers")
                        .setItems(carMakeArray, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                carMake.setText(carMakeArray[which].toString());
                                modelsArray = readCarModels(carMakeArray[which].toString());
                                model.performClick();
                            }
                        }).create().show();
            }

        });
        model.setFocusableInTouchMode(false);
        model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Select Car Model")
                        .setItems(modelsArray, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                model.setText(modelsArray[which].toString());
                                versionsArray = carVersion(modelsArray[which].toString());
                                modelVersion.performClick();
                            }
                        }).create().show();
            }

        });
        modelVersion.setFocusableInTouchMode(false);
        modelVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Select Car version")
                        .setItems(versionsArray, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                modelVersion.setText(versionsArray[which].toString());
                            }
                        }).create().show();
            }

        });

        saveVehicleDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (valid()) {
                    sqlDB = db.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    String[] userDetails=  MainActivity.getuserDeatils();
//                ByteArrayOutputStream bos = new ByteArrayOutputStream();
//                bitmapLogo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
//                byte[] img = bos.toByteArray();
                    Long tsLong = System.currentTimeMillis()/1000;
                    String ts = tsLong.toString();
                    String carId= userDetails[0]+"_"+ts;
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat sdf = new SimpleDateFormat("dd MM yyyy, h:mm a");
                    long date = System.currentTimeMillis();
                    String dateString = sdf.format(date);
                    Log.e("sdfsdf",dateString);

                    values.put("carId", carId);
                    values.put("date", dateString);
                    values.put("carMake", carMake.getText().toString());
                    values.put("model", model.getText().toString());
                    values.put("version", modelVersion.getText().toString());
                    values.put("fuelType", fuelType.getSelectedItem().toString());
                    values.put("carColour", carColour.getText().toString());
                    values.put("licensePlate", licensePlate.getText().toString());
                    values.put("notes", notes.getText().toString());
                    sqlDB.insert("vehicleDetails", null, values);
                    sqlDB.close();
                    Log.e("details of vehicle", String.valueOf(values));
                    Snackbar.make(getView(), "form valid", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    fm.popBackStack();
                }
            }
        });
        return v;
    }

    public Boolean valid() {
        if (carMake.getText().toString().trim().equalsIgnoreCase("")) {
            carMake.setError("This field can not be blank");
            Snackbar.make(getView(), "please select car maker", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (model.getText().toString().trim().equalsIgnoreCase("")) {
            model.setError("This field can not be blank");
            carMake.setError(null);
            Snackbar.make(getView(), "please select car model", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (modelVersion.getText().toString().trim().equalsIgnoreCase("")) {
            modelVersion.setError("This field can not be blank");
            modelVersion.setError(null);
            Snackbar.make(getView(), "please select car version", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (carColour.getText().toString().trim().equalsIgnoreCase("")) {
            carColour.setError("This field cannot be blank");
            modelVersion.setError(null);
            Snackbar.make(getView(), "please enter car colour", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        } else if (licensePlate.getText().toString().trim().equalsIgnoreCase("")) {
            licensePlate.setError("This field cannot be blank");
            carColour.setError(null);
            Snackbar.make(getView(), "please enter license plate number.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }/*else if (fuelType.getSelectedItem().toString().equals("Select Fuel Type")) {
            fuelType.setError("Please select material type first then select truck type");
            Snackbar.make(getView(), "Please select material type first then select truck type", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return false;
        }*/ else {
            carMake.setError(null);
            model.setError(null);
            modelVersion.setError(null);
            carColour.setError(null);
            licensePlate.setError(null);
            return true;
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String[] readCarModels(String make) {
        String[] models = new String[0];
        BufferedReader reader = null;

        try {
            // open and read the file into a StringBuilder
            InputStream in = getResources().openRawResource(R.raw.model);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                // line breaks are omitted and irrelevant
                jsonString.append(line);
            }
            // parse the JSON using JSONTokener
            array = (JSONObject) new JSONTokener(jsonString.toString()).nextValue();
            Log.e("array values", String.valueOf(array));
        } catch (FileNotFoundException e) {
            // we will ignore this one, since it happens when we start fresh
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < array.length(); i++) {

            try {
                String jsonObject = array.names().getString(i);
                if (jsonObject.contains(make))
                    list.add(array.getString(jsonObject));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i).toString();
        }
        Arrays.sort(result);
        Log.e("dsfsdfs", String.valueOf(list));
        return result;
    }

    public String[] carVersion(String make) {
        String[] versions = new String[0];
        BufferedReader reader = null;

        try {
            // open and read the file into a StringBuilder
            InputStream in = getResources().openRawResource(R.raw.versions);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                // line breaks are omitted and irrelevant
                jsonString.append(line);
            }
            // parse the JSON using JSONTokener
            versionsArrayObject = (JSONObject) new JSONTokener(jsonString.toString()).nextValue();
            Log.e("array values", String.valueOf(versionsArrayObject));
        } catch (FileNotFoundException e) {
            // we will ignore this one, since it happens when we start fresh
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < versionsArrayObject.length(); i++) {

            try {
                String jsonObject = versionsArrayObject.names().getString(i);
                if (jsonObject.contains(make))
                    list.add(versionsArrayObject.getString(jsonObject));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        String[] result = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i).toString();
        }
        Arrays.sort(result);
        Log.e("dsfsdfs", String.valueOf(list));
        return result;
    }

}
