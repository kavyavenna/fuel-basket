package creativeappsbyus.fuelbasket;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseObject extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "fuelbasket.db";
    Cursor cr;

    public DataBaseObject(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_USER_TABLE = "create table if not exists user (loginId TEXT,status INTEGER,role TEXT)";
        db.execSQL(CREATE_USER_TABLE);
        String fine_details = "create table if not exists vehicleDetails (carId TEXT,date TEXT,carMake TEXT,model TEXT,version TEXT," +
                "fuelType TEXT,carColour TEXT,licensePlate TEXT, notes TEXT)";
        db.execSQL(fine_details);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS user");
        db.execSQL("DROP TABLE IF EXISTS vehicleDetails");
        // Create tables again
        onCreate(db);
    }


}
