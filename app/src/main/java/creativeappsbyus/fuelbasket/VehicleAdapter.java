package creativeappsbyus.fuelbasket;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import creativeappsbyus.fuelbasket.R;


public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ContactViewHolder> {
    private List<ContactInfo> contactList;
    private Vehicles addVehicles;
    private Context mContext;

    public VehicleAdapter(List<ContactInfo> contactList, Vehicles addVehicles) {

        this.contactList = contactList;
        this.addVehicles = addVehicles;
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, final int i) {
            final ContactInfo ci = contactList.get(i);
        Log.e("dfsdfsdf",ci.carmake);
            contactViewHolder.carMake.setText(ci.carmake);
            contactViewHolder.modelName.setText(ci.modelName);
            contactViewHolder.carColour.setText(ci.colourofcar);
            contactViewHolder.fuelType.setText(ci.fuel);
            contactViewHolder.licencePlate.setText(ci.numplate);

    }


    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();

            View itemView = LayoutInflater.
                    from(viewGroup.getContext()).
                    inflate(R.layout.vehiclecard, viewGroup, false);
            return new ContactViewHolder(itemView);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected TextView carMake;
        protected TextView modelName;
        protected TextView fuelType;
        protected TextView carColour;
        protected TextView licencePlate;
        protected Button edit;
        protected Button del;


        public ContactViewHolder(View v) {
            super(v);
            carMake = (TextView) v.findViewById(R.id.carmakecard);
            modelName = (TextView) v.findViewById(R.id.carmodelcard);
            fuelType = (TextView) v.findViewById(R.id.fueltypecard);
            carColour = (TextView) v.findViewById(R.id.carcolourcard);
            licencePlate = (TextView) v.findViewById(R.id.licensenocard);
            del=(Button) v.findViewById(R.id.delbtn);
            edit=(Button) v.findViewById(R.id.editbtn);



        }
    }

    public static final class ContactInfo {
        protected String carmake;
        protected String modelName;
        protected String fuel;
        protected String colourofcar;
        protected String numplate;

    }

}
