package creativeappsbyus.fuelbasket;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.auth0.Auth0;
import com.auth0.android.lock.AuthenticationCallback;
import com.auth0.android.lock.Lock;
import com.auth0.android.lock.LockCallback;
import com.auth0.android.lock.utils.LockException;
import com.auth0.authentication.result.Authentication;
import com.auth0.authentication.result.UserProfile;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AddVehicles.OnFragmentInteractionListener, Vehicles.OnFragmentInteractionListener{
    public   Lock lock;
    static DataBaseObject db;
    static SQLiteDatabase sqlDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = new DataBaseObject(this);
        sqlDB = db.getReadableDatabase();
       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        Auth0 auth0 = new Auth0("lkSLPNpAW51FdpZzubbunqmQ2Lt45yvP", "kavyavenna.auth0.com");
        lock = Lock.newBuilder(auth0, callback)
                // Add parameters to the Lock Buil
                // der
                .build();

        //startActivity(lock.newIntent(MainActivity.this));
        Fragment fragment = null;
        Class fragmentClass = Vehicles.class;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    private LockCallback callback = new AuthenticationCallback() {


        @Override
        public void onAuthentication(Authentication authentication) {

        }

        @Override
        public void onCanceled() {
            // Login Cancelled response
        }

        @Override
        public void onError(LockException error){
            // Login Error response
        }
    };
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.payment) {
            // Handle the camera action
        } else if (id == R.id.vehicles) {

        } else if (id == R.id.history) {

        } else if (id == R.id.promotions) {

        }else if (id == R.id.profile) {

        } else if (id == R.id.support) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    public static String[] getuserDeatils() {
        String[] userDetails = new String[2];
        sqlDB = db.getReadableDatabase();
        Cursor cursor = sqlDB.rawQuery("SELECT * FROM user", null);
        if (cursor != null && cursor.moveToFirst()) {
            userDetails[0] = cursor.getString(cursor.getColumnIndex("loginId"));
            userDetails[1] = cursor.getString(cursor.getColumnIndex("status"));
            cursor.close();
            sqlDB.close();
            return userDetails;
        } else {
            return userDetails;
        }
    }
    public static String[][] getvehicleDetails() {

        sqlDB = db.getReadableDatabase();
        Cursor cursor = sqlDB.rawQuery("SELECT * FROM vehicleDetails", null);
        String[][]  array = new String[cursor.getCount()][];
        int i = 0;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String[] vehicleDetails = new String[9];
            vehicleDetails[0] = cursor.getString(cursor.getColumnIndex("carId"));
            vehicleDetails[1] = cursor.getString(cursor.getColumnIndex("carMake"));
            vehicleDetails[2] = cursor.getString(cursor.getColumnIndex("model"));
            vehicleDetails[3] = cursor.getString(cursor.getColumnIndex("version"));
            vehicleDetails[4] = cursor.getString(cursor.getColumnIndex("fuelType"));
            vehicleDetails[5] = cursor.getString(cursor.getColumnIndex("carColour"));
            vehicleDetails[6] =cursor.getString(cursor.getColumnIndex("licensePlate"));
            vehicleDetails[7] = cursor.getString(cursor.getColumnIndex("notes"));
            vehicleDetails[8] = cursor.getString(cursor.getColumnIndex("date"));
            array[i] = vehicleDetails;
            i++;
            cursor.moveToNext();
        }

        cursor.close();
        sqlDB.close();
        return array;

    }
}
