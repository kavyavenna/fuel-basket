package creativeappsbyus.fuelbasket;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import creativeappsbyus.fuelbasket.AddVehicles;
import creativeappsbyus.fuelbasket.MainActivity;
import creativeappsbyus.fuelbasket.R;
import creativeappsbyus.fuelbasket.VehicleAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Vehicles.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Vehicles#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Vehicles extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
private Button addVehiclebtn;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public ScrollView scrollView;
    VehicleAdapter adapter;
    RecyclerView recyclerView;
    Vehicles vehicles;
    creativeappsbyus.fuelbasket.AddVehicles addVehicles;
    FragmentActivity c;
    private OnFragmentInteractionListener mListener;

    public Vehicles() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Vehicles.
     */
    // TODO: Rename and change types and number of parameters
    public static Vehicles newInstance(String param1, String param2) {
        Vehicles fragment = new Vehicles();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
    View v= inflater.inflate(R.layout.fragment_vehicles, container, false);
addVehiclebtn= (Button) v.findViewById(R.id.addVehicle);
       // ListView list=(ListView) v.findViewById(R.id.vehicle_list);
        recyclerView = (RecyclerView) v.findViewById(R.id.cardList1);
        LinearLayoutManager layoutManager = new LinearLayoutManager(c);
        recyclerView.setLayoutManager(layoutManager);
        vehicles = this;
        c = getActivity();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        scrollView = (ScrollView) v.findViewById(R.id.scrollViewSearch);
        scrollView.setSmoothScrollingEnabled(true);
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                if (scrollY == 0) mSwipeRefreshLayout.setEnabled(true);
                else mSwipeRefreshLayout.setEnabled(false);
            }
        });
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new VehicleAdapter(createList(), vehicles);
                        c.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setAdapter(adapter);

                            }
                        });
                    }
                }).start();
            }
        });

        addVehiclebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                AddVehicles nextFrag = new AddVehicles();
                Bundle args = new Bundle();
                nextFrag.setArguments(args);
                android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.flContent, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });



        return v;
    }
    public List<VehicleAdapter.ContactInfo> createList(){
        final List<VehicleAdapter.ContactInfo> result = new ArrayList<VehicleAdapter.ContactInfo>();
        String[][] vehicleDetails= MainActivity.getvehicleDetails();
      //  Log.e("sdfsdf",vehicleDetails[0][0]);
        for (String[] vehicles : vehicleDetails) {
            VehicleAdapter.ContactInfo ci = new VehicleAdapter.ContactInfo();
            ci.carmake =vehicles[1];
            Log.e("vehicle log",vehicles[1]);
            ci.modelName =vehicles[2];
            ci.fuel =vehicles[4];
            ci.colourofcar =vehicles[5];
            ci.numplate =vehicles[6];
            result.add(ci);
        }
        return result;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
